TrojaNow Readme

=======App=======
1. Android 5.1 is required
2. Since the server doesn't have fixed IP, we need to modeify IP after the server deployed. It can be done easily by editing Config.java.


======Server======
1. MySQL database is required
2. Database name: trojanow
3. Create table:
	CREATE TABLE `status` (
 		`ID` int(11) NOT NULL AUTO_INCREMENT,
 		`statusUsername` varchar(255) DEFAULT NULL,
  		`statusDate` varchar(255) DEFAULT NULL,
 		`statusContent` varchar(255) DEFAULT NULL,
 		`statusEnvironment` varchar(255) DEFAULT NULL,
  		`statusTarget` varchar(255) DEFAULT NULL,
  		PRIMARY KEY (`ID`)
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

	CREATE TABLE `userinf` (
  		`userName` varchar(255) NOT NULL,
  		`userPassword` varchar(255) NOT NULL,
  		PRIMARY KEY (`userName`,`userPassword`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;

	CREATE TABLE `comment` (
		`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
		`statusID` int(11) NOT NULL,
		`commentUsername` varchar(255) DEFAULT NULL,
		`commentDate` varchar(255) DEFAULT NULL,
		`commentContent` varchar(255) DEFAULT NULL,
		PRIMARY KEY (`ID`)
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


4. The url, username and password of database can be modified in Config.java.