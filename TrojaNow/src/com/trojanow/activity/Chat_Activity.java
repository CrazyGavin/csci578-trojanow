package com.trojanow.activity;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.trojanow.R;
import com.trojanow.util.ChatUpdateThread;
import com.trojanow.util.MyMessage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Chat_Activity extends Activity implements OnClickListener {
	
	/** Properties **/  
    private Button Chat_Message;
    private Button Chat_refresh;
    private Button Chat_submit;
    private TextView Chat_display;
    private TextView Chat_show_username;
    private EditText Chat_target;
    private EditText Chat_content;
    private Handler handler;
    private Thread Chat_thread;
    
    private static ObjectOutputStream oos;
    private static ObjectInputStream ois;
    
    private static String username;
    private static Socket Chat_socket = null; 
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_chat);
      
      if (android.os.Build.VERSION.SDK_INT > 9) {
          StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
          StrictMode.setThreadPolicy(policy);
      }

      // Connect interface elements to properties   
      Chat_target =  (EditText)findViewById(R.id.Chat_target); 
      Chat_content = (EditText)findViewById(R.id.Chat_content); 
      Chat_display = (TextView)findViewById(R.id.Chat_display);
      Chat_show_username = (TextView)findViewById(R.id.Chat_show_username);
      Chat_Message = (Button)findViewById(R.id.Chat_Message);
      Chat_refresh = (Button)findViewById(R.id.Chat_refresh);
      Chat_submit = (Button)findViewById(R.id.Chat_submit);

      Chat_display.setMovementMethod(ScrollingMovementMethod.getInstance()); 
      
      // Setup ClickListeners
      Chat_Message.setOnClickListener(this);
      Chat_refresh.setOnClickListener(this);
      Chat_submit.setOnClickListener(this);
      
      // Set the initial values        
      Chat_show_username.setText("Welcome back, " + username);
      handler = new Handler(){
    	@Override
    	public void handleMessage(Message msg){
    		if(msg.what == 0x234){
    			Chat_display.append("\n" + msg.obj.toString());
    		}
    	}
      };
      
      try{
    	  Chat_thread = new Thread(new ChatUpdateThread(ois, handler));
    	  Chat_thread.start();
      	}catch (Exception e){
      		e.printStackTrace();
      	}
        MyMessage Chat_enter = new MyMessage("chat",2);
		Chat_enter.argv.add("enter");
		Chat_enter.argv.add(username);
    	 try{
  		  oos.writeObject(Chat_enter);
  		  oos.flush();
  	  }catch(Exception e){
  		  e.printStackTrace();
  	  }
      
      }
    
    /** Methods **/
    
    //set socket  
    protected static void set_socket(Socket input){
    	Chat_socket = input;
    }
    
    //set stream
    protected static void set_stream(ObjectOutputStream input_oos, ObjectInputStream input_ois){
  	  oos = input_oos;
  	  ois = input_ois;
    }
    
    //set username
    protected static void set_username(String input){
  	  	username = input;
    }
    
    //return username
    protected static String get_username(){
  	  	return username;
    }
    
    //buttons on click
    public void onClick(View v) {
      if(v == Chat_Message){
      	Main_Activity.set_username(username);
      	Main_Activity.set_socket(Chat_socket);
      	Main_Activity.set_stream(oos, ois);
      	try{
          	Chat_thread.interrupt();
      		
      		MyMessage Chat_exit = new MyMessage("chat",2);
      		Chat_exit.argv.add("exit");
      		Chat_exit.argv.add(username);
          	 try{
        		  oos.writeObject(Chat_exit);
        		  oos.flush();
        	  }catch(Exception e){
        		  e.printStackTrace();
        	  }
          	 
          	Chat_thread.join();     
      	}catch (Exception e){
      		e.printStackTrace();
      	}
      	startActivity(new Intent(Chat_Activity.this, Main_Activity.class));
      	finish();
      }
      if(v == Chat_refresh){
        	Chat_Activity.set_username(username);
        	Chat_Activity.set_socket(Chat_socket);
        	Chat_Activity.set_stream(oos, ois);
          	try{
              	Chat_thread.interrupt();
          		
          		MyMessage Chat_exit = new MyMessage("chat",2);
          		Chat_exit.argv.add("exit");
          		Chat_exit.argv.add(username);
	          	 try{
	        		  oos.writeObject(Chat_exit);
	        		  oos.flush();
	        	  }catch(Exception e){
	        		  e.printStackTrace();
	        	  }
	          	 
	          	Chat_thread.join();      	 
          	}catch (Exception e){
          		e.printStackTrace();
          	}
        	startActivity(new Intent(Chat_Activity.this, Chat_Activity.class));
          	finish();
      }
      
      if(v == Chat_submit){
    	  MyMessage Chat_message = new MyMessage("chat",3);
    	  Chat_message.argv.add(Chat_target.getText().toString());
    	  Chat_message.argv.add(username);
    	  Chat_message.argv.add(username + ": \t" +Chat_content.getText().toString());
    	  if(Chat_content.getText().toString().length() == 0){
    		  return;
    	  }
    	  try{
    		  oos.writeObject(Chat_message);
    		  oos.flush();
    	  }catch(Exception e){
    		  e.printStackTrace();
    	  }
    	  Chat_content.setText("");
      }
    }
}
