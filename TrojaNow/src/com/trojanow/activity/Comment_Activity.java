package com.trojanow.activity;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;

import com.trojanow.R;
import com.trojanow.R.id;
import com.trojanow.R.layout;
import com.trojanow.util.ChatUpdateThread;
import com.trojanow.util.MyMessage;
import com.trojanow.util.Status;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Comment_Activity extends Activity implements OnClickListener {
	
	/** Properties **/ 
	private static Status curStatus;
	
	private Button Comment_Back;
	private Button Comment_Refresh;
	private Button Comment_Like;
	private Button Comment_Dislike;
	private Button Comment_Submit;
	private TextView Comment_show_username;
	private TextView Comment_Date;
//	private TextView Comment_Environment;
	private TextView Comment_Username;
	private TextView Comment_Status;
//	private TextView Comment_Category;
	private EditText Comment_newcontent;
	
	private TextView Comment_Content;
	
    private static ObjectOutputStream oos;
    private static ObjectInputStream ois;
    
    private static String username;
    private static Socket Comment_socket = null; 
    
    private SimpleDateFormat sDateFormat;
	
    private int likeCount = 0;
    private int dislikeCount = 0;
    
    /** Called when the activity is first created. */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			setContentView(R.layout.activity_comment);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	      if (android.os.Build.VERSION.SDK_INT > 9) {
	          StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	          StrictMode.setThreadPolicy(policy);
	      }
	      
	   // Connect interface elements to properties 
	      Comment_Back = (Button)findViewById(R.id.Comment_Back);
	      Comment_Refresh = (Button)findViewById(R.id.Comment_refresh);
	      Comment_Like = (Button)findViewById(R.id.Comment_up);
	      Comment_Dislike = (Button)findViewById(R.id.Comment_down);
	      Comment_Submit = (Button)findViewById(R.id.Comment_submit);
	      Comment_Date = (TextView)findViewById(R.id.Comment_date);
//	      Comment_Environment = (TextView)findViewById(R.id.Comment_environment);
	      Comment_Username = (TextView)findViewById(R.id.Comment_username);
	      Comment_Status = (TextView)findViewById(R.id.Comment_status);
//	      Comment_Category = (TextView)findViewById(R.id.Comment_category);
	      Comment_Content = (TextView)findViewById(R.id.Comment_content);
	      Comment_show_username  = (TextView)findViewById(R.id.Comment_show_username);
	      Comment_newcontent  = (EditText)findViewById(R.id.Comment_newcontent);
	      
	      Comment_Content.setMovementMethod(ScrollingMovementMethod.getInstance());
	      
	    // Setup ClickListeners
	      Comment_Back.setOnClickListener(this);
	      Comment_Refresh.setOnClickListener(this);
	      Comment_Like.setOnClickListener(this);
	      Comment_Dislike.setOnClickListener(this);
	      Comment_Submit.setOnClickListener(this);
	      
	    // Set the initial values
	      Comment_show_username.setText("Welcome back, " + username);
	      sDateFormat    =   new    SimpleDateFormat("MM/dd/yyyy    hh:mm:ss");  
	      showStatus();
	      showComments();
	      showLikes();
	}
    
    /** Methods **/
    
    //set socket  
    protected static void set_socket(Socket input){
    	Comment_socket = input;
    }
    
    //set stream
    protected static void set_stream(ObjectOutputStream input_oos, ObjectInputStream input_ois){
  	  oos = input_oos;
  	  ois = input_ois;
    }
    
    //set username
    protected static void set_username(String input){
  	  	username = input;
    }
    
    //return username
    protected static String get_username(){
  	  	return username;
    }
    
    //set curStatus
    public static void set_curstatus(Status input){
  	  curStatus = input;
    }
        
    //show status
    private void showStatus(){
    	Comment_Date.setText(curStatus.getDate().toString());
//    	Comment_Environment.setText(curStatus.getEnvironment().toString());
    	Comment_Username.setText(curStatus.getUsername().toString());
    	Comment_Status.setText(curStatus.getContent().toString());
//        if(curStatus.getTarget().toLowerCase().equals("all")){
//        	Comment_Category.setText("Public");
//        }else{
//        	Comment_Category.setText("Private");
//        }
    }
    
    private void showComments(){
    	Comment_Content.setText(getComments());
    	if(Comment_Content.getText().toString().length() == 0){
    		Comment_Content.setText("No comment exist. Try to create the first one!");
    	}
    }
    
    //update like_count
    private void likeCount(){
		MyMessage Comment_message = new MyMessage("comment_count",2);
		MyMessage Comment_response = null;
		Comment_message.argv.add(curStatus.getID() + "");	
		Comment_message.argv.add("=like=");	
		try{
			oos.writeObject(Comment_message);
			oos.flush();
			Comment_response = (MyMessage)ois.readObject();
			
			if(Comment_response.type.equals("comment_count_response") && Comment_response.args == 2 && Comment_response.argv.get(0).equals("true")){
				likeCount = Integer.parseInt(Comment_response.argv.get(1));
			}
			else{
				return ;
			}
		}catch(Exception e){
			e.printStackTrace();
			return ;
		}
    }
    
    //update dislike_count
    private void dislikeCount(){
		MyMessage Comment_message = new MyMessage("comment_count",2);
		MyMessage Comment_response = null;
		Comment_message.argv.add(curStatus.getID() + "");	
		Comment_message.argv.add("=dislike=");	
		try{
			oos.writeObject(Comment_message);
			oos.flush();
			Comment_response = (MyMessage)ois.readObject();
			
			if(Comment_response.type.equals("comment_count_response") && Comment_response.args == 2 && Comment_response.argv.get(0).equals("true")){
				dislikeCount = Integer.parseInt(Comment_response.argv.get(1));
			}
			else{
				return ;
			}
		}catch(Exception e){
			e.printStackTrace();
			return ;
		}
    }

    //show like
    private  void showLikes(){
    	likeCount();
    	dislikeCount();
    	Comment_Like.setText("Like(" + likeCount + ")");
    	Comment_Dislike.setText("Dislike(" + dislikeCount + ")");
    }
    
    //check if the comment is valid
    private boolean checkContent(){
    	if(Comment_newcontent.getText().toString().length()>0 && Comment_newcontent.getText().toString().length()<255){
    		return true;
    	}
    	return false;
    }
    
    //puvlish the comment
    private boolean publishComment(String content){
		MyMessage Comment_message = new MyMessage("comment_submit",4);
		MyMessage Comment_response = null;
		Comment_message.argv.add(curStatus.getID()+"");
		Comment_message.argv.add(username);
		Comment_message.argv.add(sDateFormat.format(new    java.util.Date()));
		Comment_message.argv.add(content);
		try{
			oos.writeObject(Comment_message);
			oos.flush();
			
			Comment_response = (MyMessage)ois.readObject();
			
			if(Comment_response.type.equals("comment_submit_response") && Comment_response.args == 1 && Comment_response.argv.get(0).equals("true")){
				return true;
			}
			else{
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
    }
    
    //get Comments
    private String getComments(){
		MyMessage Comment_message = new MyMessage("comment_get",1);
		MyMessage Comment_response = null;
		Comment_message.argv.add(curStatus.getID() + "");
		
		try{
			oos.writeObject(Comment_message);
			oos.flush();
			Comment_response = (MyMessage)ois.readObject();
			
			if(Comment_response.type.equals("comment_get_response") && Comment_response.args == 2 && Comment_response.argv.get(0).equals("true")){
				return Comment_response.argv.get(1).toString();
			}
			else{
				return null;
			}
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
    }
    
	@Override
	public void onClick(View v) {
		if(v == Comment_Back){
        	Main_Activity.set_username(username);
        	Main_Activity.set_socket(Comment_socket);
			Main_Activity.set_stream(oos, ois);
			startActivity(new Intent(Comment_Activity.this, Main_Activity.class));
        	finish();
		}
		
		if(v == Comment_Refresh){
        	Comment_Activity.set_username(username);
        	Comment_Activity.set_socket(Comment_socket);
			Comment_Activity.set_stream(oos, ois);
			Comment_Activity.set_curstatus(curStatus);
			startActivity(new Intent(Comment_Activity.this, Comment_Activity.class));
        	finish();
		}
		
		if(v == Comment_Submit){
			if(checkContent()){
				if(publishComment(Comment_newcontent.getText().toString())){
					Comment_newcontent.setText("");
					showComments();
				}
			}
		}
		
		if(v == Comment_Like){
			if(publishComment("=like=")){
				Comment_newcontent.setText("");
				showComments();
				showLikes();
			}
		}
		
		if(v == Comment_Dislike){
			if(publishComment("=dislike=")){
				Comment_newcontent.setText("");
				showComments();
				showLikes();
			}
		}
	}

}
