package com.trojanow.activity;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.trojanow.R;
import com.trojanow.util.MyMessage;
import com.trojanow.util.Config;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Login_Activity extends Activity implements OnClickListener {
  /** Properties **/
	private String IP = Config.Server_IP;
	
  private String username = "test";
	
  private Button Login_userlogin;
  private Button Login_signup;
  
  private static ObjectOutputStream oos;
  private static ObjectInputStream ois;
  
  private EditText Login_username;
  private EditText Login_password;
  
  private TextView Login_result;

  private String Login_input_password = "";
  private String Login_input_username = "";
  
  private static Socket Login_socket = null;  
  
  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    
    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
    
    if (android.os.Build.VERSION.SDK_INT > 9) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    // Connect interface elements to properties   
    Login_username = (EditText)findViewById(R.id.Login_username);
    Login_password = (EditText)findViewById(R.id.Login_password); 
    Login_result = (TextView)findViewById(R.id.Login_result);
    Login_userlogin = (Button)findViewById(R.id.Login_userlogin);
    Login_signup = (Button)findViewById(R.id.Login_signup);

    // Setup ClickListeners
    Login_userlogin.setOnClickListener(this);
    Login_signup.setOnClickListener(this);
    
    // Set the initial brew values
    
    while(true){
	    try {  
	    	Login_socket = new Socket(IP, 20000); 
	    } catch (Exception e) {  
	    	Login_result.setText("Fail to connect server!");
		    try{
		    	Thread.sleep(1000);
		    }catch (Exception ep){
		    	ep.printStackTrace();
		    }  
	    	continue;
	    } 	    
	    Login_result.setText("Connect succeed!");
	    break;
    }
    
    try{
    	oos = new ObjectOutputStream(Login_socket.getOutputStream());
    	ois = new ObjectInputStream(Login_socket.getInputStream());
    }catch(Exception e){
    	e.printStackTrace();
    }
    
  }
  
  /** Methods **/
  //set socket  
  protected static void set_socket(Socket input){
  	Login_socket = input;
  }
  
  //set stream
  protected static void set_stream(ObjectOutputStream input_oos, ObjectInputStream input_ois){
	  oos = input_oos;
	  ois = input_ois;
  }
  
  //get username and password
  private void get_input(){
	  Login_input_username = Login_username.getText().toString();
	  Login_input_password = Login_password.getText().toString();
	  
	  username = Login_input_username;
  }
  
  //try to login(test)
  private boolean try_login(){	
	  
	  MyMessage Login_message = new MyMessage("login",2);
	  MyMessage Login_response = null;
	  Login_message.argv.add(Login_input_username);
	  Login_message.argv.add(Login_input_password);
	  
	  try{
		  oos.writeObject(Login_message);
		  oos.flush();
		  
		  Login_response =  (MyMessage)ois.readObject();
		  
		  if(Login_response.type.equals("login_response") && Login_response.args == 2 && Login_response.argv.get(0).equals("true")){
			  return true;
		  }
		  else if(Login_response.type.equals("login_response") && Login_response.args == 2 && Login_response.argv.get(0).equals("false")){
			  Login_result.setText(Login_response.argv.get(1).toString());
			  return false;
		  }
		  
	  }catch (Exception e){
		  e.printStackTrace();
		  return false;
	  }
	  
	  return false;
  }
  
  //buttons on click
  public void onClick(View v) {
    if(v == Login_userlogin){
    	get_input();
    	
    	if(Login_input_username.indexOf("\'") != -1 || Login_input_username.indexOf("\"") != -1){
    		Login_result.setText("Invalid username!");
    		Login_password.setText("");
    		return;
    	}
    	
		if(Login_input_username.indexOf("\'") != -1 || Login_input_username.indexOf("\"") != -1){
    		Login_result.setText("Invalid password!");
    		Login_password.setText("");
    		return;
		}
    	
    	if(try_login()){
    		Login_result.setText("Login Success!");    		
    		Handler handler = new Handler(); 		
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {                     
                	Main_Activity.set_username(username);
                	Main_Activity.set_socket(Login_socket);
                	Main_Activity.set_stream(oos, ois);
                	startActivity(new Intent(Login_Activity.this, Main_Activity.class));
                	finish();
                }
            }, 3000);   		
    	}else{
//    		Login_result.setText("Login Failed!");
    		Login_password.setText("");
    	}
    }
    
    if(v == Login_signup){
    	Register_Activity.set_socket(Login_socket);
    	Register_Activity.set_stream(oos, ois);
    	startActivity(new Intent(Login_Activity.this, Register_Activity.class));
    	finish();
    }
  }
}
