package com.trojanow.activity;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
 
import com.trojanow.R;
import com.trojanow.util.ListViewAdapter;
import com.trojanow.util.MyMessage;
import com.trojanow.util.Status;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
 
public class Main_Activity extends ListActivity implements OnScrollListener, OnClickListener {
	
	/** Properties **/
    private int visibleLastIndex = 0;   
    private int visibleItemCount;       
    private ListViewAdapter adapter;
    private ListView Main_listview;
    private View Main_loadmoreview;
    private Button Main_loadmorebutton;
    private Button Main_publishbutton;
    private Button Main_refresh;
    private Button Main_chat;
    private TextView Main_show_username;
    private Handler Main_handler = new Handler(); 
    private static String username;    
//    public static News Main_latestnews = null;
    
    private static Socket Main_socket = null;  
 
	private static ObjectOutputStream oos;
	private static ObjectInputStream ois;
    
	private int IDRange;
	
	private static Handler handler;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         
        Main_loadmoreview = getLayoutInflater().inflate(R.layout.load_more, null);
        setContentView(R.layout.activity_main);
        
	    if (android.os.Build.VERSION.SDK_INT > 9) {
	        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);
	    }
        
     // Connect interface elements to properties 
        Main_loadmorebutton = (Button) Main_loadmoreview.findViewById(R.id.Main_loadmore);
        Main_publishbutton = (Button) findViewById(R.id.Main_publish);
        Main_refresh = (Button)findViewById(R.id.Main_refresh);
        Main_chat = (Button)findViewById(R.id.Main_chat);
        
        Main_show_username = (TextView)findViewById(R.id.Main_show_username);
        
        Main_listview = getListView();               
        Main_listview.addFooterView(Main_loadmoreview);   
 
        initAdapter();
        setListAdapter(adapter);                    
        Main_listview.setOnScrollListener(this);     
        
     // Setup ClickListeners
        Main_loadmorebutton.setOnClickListener(this);
        Main_publishbutton.setOnClickListener(this);
        Main_refresh.setOnClickListener(this);
        Main_chat.setOnClickListener(this);
     
     // Set the initial brew values
        Main_show_username.setText("Welcome back, " + username);
        handler = new Handler(){
        	@Override
        	public void handleMessage(Message msg){
        		if(msg.what == 0x234){
        			if(msg.obj.toString().equals("comment")){
        	        	Comment_Activity.set_username(username);
        	        	Comment_Activity.set_socket(Main_socket);
        	        	Comment_Activity.set_stream(oos, ois);
        	        	startActivity(new Intent(Main_Activity.this, Comment_Activity.class));
        	        	finish();
        			}
        		}
        	}
          };
    }
    
    /** Methods **/    
    //get handler
    public static Handler get_handler(){
    	return handler;
    }
    
    //set socket  
    protected static void set_socket(Socket input){
    	Main_socket = input;
    }
    
    //set stream
    protected static void set_stream(ObjectOutputStream input_oos, ObjectInputStream input_ois){
  	  oos = input_oos;
  	  ois = input_ois;
    }
    
    //set username
    protected static void set_username(String input){
  	  	username = input;
    }
    
    //return username
    protected static String get_username(){
  	  	return username;
    }

    //init adapter
    private void initAdapter() {
    	IDRange = Integer.MAX_VALUE;
        adapter = new ListViewAdapter(this, new ArrayList<Status>());
        loadData();
    }
 

    //when scroll
    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.visibleItemCount = visibleItemCount;
        visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
    }
 
    //Scroll state changed
    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        int itemsLastIndex = adapter.getCount() - 1;    
        int lastIndex = itemsLastIndex + 1;            
        if (scrollState == OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == lastIndex) {
            Log.i("LOADMORE", "loading...");
        }
    }
     

    //click loadMore button
    public void loadMore(View view) {
        Main_loadmorebutton.setText("loading..."); 
        Main_handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                 
                loadData();
                 
                adapter.notifyDataSetChanged(); 
                Main_listview.setSelection(visibleLastIndex - visibleItemCount + 1); 
                 
                Main_loadmorebutton.setText("load more");
            }
        }, 500);
    }
    
    //load data
    private void loadData() {	       
  	  MyMessage Main_message = new MyMessage("mainLoadData", 2);
  	  MyMessage Main_response = null;
  	  Main_message.argv.add(String.valueOf(IDRange));
  	  Main_message.argv.add(username);

	  try{
		  oos.writeObject(Main_message);
		  oos.flush();
		  
		  Main_response =  (MyMessage)ois.readObject();
		  
		  if(!Main_response.type.equals("loadData_response") && Main_response.args != 1 && !Main_response.argv.get(0).equals("true")){
			  return;
		  }
		  else{
			  for(Status item : Main_response.argvNews){
				  IDRange = Math.min(IDRange, item.getID());
				  adapter.addItem(item);
			  }
		  }  
	  }catch (Exception e){
		  e.printStackTrace();
	  }      
    }
    
    //buttons on click
    public void onClick(View v) {
        if(v == Main_loadmorebutton){
        	loadMore(v);
        }

        if(v == Main_publishbutton){
        	Publish_Activity.set_username(username);
        	Publish_Activity.set_socket(Main_socket);
        	Publish_Activity.set_stream(oos, ois);
        	startActivity(new Intent(Main_Activity.this, Publish_Activity.class));
        	finish();
        }

        if(v == Main_refresh){
        	Main_Activity.set_username(username);
        	Main_Activity.set_socket(Main_socket);
			Main_Activity.set_stream(oos, ois);
			startActivity(new Intent(Main_Activity.this, Main_Activity.class));
        	finish();
        }
        
        if(v == Main_chat){
        	Chat_Activity.set_username(username);
        	Chat_Activity.set_socket(Main_socket);
        	Chat_Activity.set_stream(oos, ois);
        	startActivity(new Intent(Main_Activity.this, Chat_Activity.class));
          	finish();
        }
        
      }
}
