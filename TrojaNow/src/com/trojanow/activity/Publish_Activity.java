package com.trojanow.activity;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;

import com.trojanow.R;
import com.trojanow.util.MyMessage;
import com.trojanow.util.Status;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class Publish_Activity extends Activity implements OnClickListener {
	
	/** Properties **/
	private Button Publish_submit;
	private Button Publish_back;	
	private EditText Publish_content;
	private EditText Publish_target;
	private CheckBox Publish_environment;
	private CheckBox Publish_anonymous;
	private TextView Publish_result;
	private SimpleDateFormat sDateFormat;
	private Handler Main_handler = new Handler();
	
	private static String username;
	
//	private String err = "";
	private String temperature = "";
	
	private static Socket Publish_socket = null;  
	
	private static ObjectOutputStream oos;
	private static ObjectInputStream ois;
	
	private SensorManager Publish_sensorManager = null;
	private Sensor Publish_temperatureSensor = null;
	private SensorEventListener Publish_sensorListener = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_publish);
		
	    if (android.os.Build.VERSION.SDK_INT > 9) {
	        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);
	    }
		
		// Connect interface elements to properties 
		Publish_submit = (Button)findViewById(R.id.Publish_submit);
		Publish_back = (Button)findViewById(R.id.Publish_back);
		Publish_content = (EditText)findViewById(R.id.Publish_content);
		Publish_target = (EditText)findViewById(R.id.Publish_target);
		Publish_environment = (CheckBox)findViewById(R.id.Publish_environment);
		Publish_anonymous = (CheckBox)findViewById(R.id.Publish_anonymous);
		
		Publish_result = (TextView)findViewById(R.id.Publish_result);
		
	    // Setup ClickListeners
		Publish_submit.setOnClickListener(this);
		Publish_back.setOnClickListener(this);
		// Set the initial values
		sDateFormat    =   new    SimpleDateFormat("MM/dd/yyyy    hh:mm:ss");  
		Publish_sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		Publish_temperatureSensor = Publish_sensorManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE);
		Publish_sensorListener = new SensorEventListener(){
			public void onSensorChanged(SensorEvent event){
				if(event.sensor.getType() == Sensor.TYPE_TEMPERATURE){
					float temp = event.values[0];
					temperature = temp + "��C";
				}
			}
			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy) {
				// TODO Auto-generated method stub		
			}
		};
		
		Publish_sensorManager.registerListener(Publish_sensorListener, Publish_temperatureSensor, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	  /** Methods **/	
    
	//set socket  
    protected static void set_socket(Socket input){
    	Publish_socket = input;
    }
	
    //set stream
    protected static void set_stream(ObjectOutputStream input_oos, ObjectInputStream input_ois){
  	  oos = input_oos;
  	  ois = input_ois;
    }
    
	//set username
    protected static void set_username(String input){
  	  	username = input;
    }
    
    //get username
    protected static String get_username(){
  	  	return username;
    }
    
	//publish to server
    //to be implemented
	private boolean Publish(Status input){	
		
//		Main_Activity.Main_latestnews = input.copy();
		
		  MyMessage Publish_message = new MyMessage("publish",1);
		  MyMessage Publish_response = null;
		  Publish_message.argvNews.add(input);
		
		  try{
			  oos.writeObject(Publish_message);
			  oos.flush();
			  
			  Publish_response =  (MyMessage)ois.readObject();
			  
			  if(Publish_response.type.equals("publish_response") && Publish_response.args == 1 && Publish_response.argv.get(0).equals("true")){
				  return true;
			  }else{
				  return false;
			  }
		  }catch (Exception e){
			  e.printStackTrace();
			  return false;
		  }
	}
	
	//check content (valid?)
	private boolean checkContent(){
		String temp = Publish_content.getText().toString();
		if(temp.length() >= 140){
			Publish_result.setText("You think too much.... (longer than 140 characters)");
			return false;	//too long
		}
		
		return true;
	}
	
	//get environment information
	//to be implemented
	private String getEnvironment(){	
		String result = "";
		if(temperature.length() != 0){
			result += temperature + '\n';
		}else{
//			err = "Can't detect temperature Sensor!";
		}
		
		return result;
	}
	
	private Status prepareNews(){
		String date = sDateFormat.format(new    java.util.Date());
		String content = Publish_content.getText().toString();
		String target_t = Publish_target.getText().toString();
		if(target_t.equals("")){
			target_t = "all";
		}
		if(target_t.toLowerCase().equals("all")){
			target_t = target_t.toLowerCase();
		}
		
		String username_t;	
		if(Publish_anonymous.isChecked() || username.equals(""))
			username_t = "Anonymous_" + username;
		else{
			username_t = username;
		}
		
		String environment_t;
		if(Publish_environment.isChecked()){
			environment_t = getEnvironment();
		}
		else{
			environment_t = "";
		}
		
		return (new Status(username_t, date, content, environment_t, target_t));
	}
	

	//buttons on click 
	public void onClick(View v) {
		if(v == Publish_submit){
			
			if(checkContent()){
				
				Publish(prepareNews());
				
				Publish_result.setText("Publish Succeed!");
				
//				if(err.length() > 0){
//					Publish_result.setText(err);
//				}
				
		        Main_handler.postDelayed(new Runnable() {
		            @Override
		            public void run() {
		    			startActivity(new Intent(Publish_Activity.this, Main_Activity.class));
		    			Main_Activity.set_username(username);
		    			finish();
		            }
		        }, 1000);
		        
			}else{
//				Publish_result.setText("Something Wrong....!");
			}
			
		}
		if(v == Publish_back){
			
			Publish_sensorManager.unregisterListener(Publish_sensorListener, Publish_temperatureSensor);
			
			Main_Activity.set_username(username);
			Main_Activity.set_socket(Publish_socket);
			Main_Activity.set_stream(oos, ois);
			startActivity(new Intent(Publish_Activity.this, Main_Activity.class));
			finish();
		}
		
	}
}
