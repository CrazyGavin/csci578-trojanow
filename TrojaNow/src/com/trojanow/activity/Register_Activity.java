package com.trojanow.activity;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.trojanow.R;
import com.trojanow.util.MyMessage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Register_Activity extends Activity implements OnClickListener {
		/** Properties **/
	  private Button Register_join;
	  private Button Register_back;
	  
	  private EditText Register_username;
	  private EditText Register_password;
	  private EditText Register_confirm_password;
	  
	  private TextView Register_result;

	  private static ObjectOutputStream oos;
	  private static ObjectInputStream ois;
	  
	  private String Register_input_username = "";
	  private String Register_input_password = "";
	  private String Register_input_password_confirm = "";
	
	  private static Socket Register_socket = null;  
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
	    if (android.os.Build.VERSION.SDK_INT > 9) {
	        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);
	    }
		
		// Connect interface elements to properties 
		Register_join = (Button)findViewById(R.id.Register_join);
		Register_back = (Button)findViewById(R.id.Register_back);
		Register_username = (EditText)findViewById(R.id.Register_username);
		Register_password = (EditText)findViewById(R.id.Register_password);
		Register_confirm_password = (EditText)findViewById(R.id.Register_confirm_password);
		Register_result = (TextView)findViewById(R.id.Register_result);
		
	    // Setup ClickListeners
		Register_join.setOnClickListener(this);
		Register_back.setOnClickListener(this);
	    // Set the initial brew values
	}
	
	  /** Methods **/
	
    //set socket
    protected static void set_socket(Socket input){
    	Register_socket = input;
    }
    
    //set stream
    protected static void set_stream(ObjectOutputStream input_oos, ObjectInputStream input_ois){
  	  oos = input_oos;
  	  ois = input_ois;
    }
	
	//get input
	 private void get_input(){
		 Register_input_username = Register_username.getText().toString();
		 Register_input_password = Register_password.getText().toString();
		 Register_input_password_confirm = Register_confirm_password.getText().toString();
	  }
	
	 //check input (valid?)
	 //username can't be NULL
	 //password should be same
	 //password should be at least 6 characters
	private boolean check_input(){
		get_input();
		if(Register_input_username.indexOf("\'") != -1 || Register_input_username.indexOf("\"") != -1){
			Register_result.setText("Username can't not contain \" or \' !");
			return false;
		}
		
		if(Register_input_password.indexOf("\'") != -1 || Register_input_password.indexOf("\"") != -1){
			Register_result.setText("Password can't not contain \" or \' !");
			return false;
		}
		
		int check_username_result = check_username();
		if(check_username_result == -1){
			Register_result.setText("Username can't not be NULL!");
			return false;
		}
		
		if(!Register_input_password.equals(Register_input_password_confirm)){
			Register_result.setText("Passwords are not match!");
			Register_password.setText("");
			Register_confirm_password.setText("");
			return false;
		}
		
		if(Register_input_password.length() < 6){
			Register_result.setText("Passwords must contain at least 6 characters!");
			Register_password.setText("");
			Register_confirm_password.setText("");
			return false;
		}
		return true;
	}
	
	//check username
	private int check_username(){	
		if(Register_input_username.length() == 0)
			return -1;
		
		return 1;
	}
	
	//register to the server
	//to be implemented
	private boolean commit_register(){
		
		MyMessage Register_message = new MyMessage("register",2);
		MyMessage Register_response = null;
		
		Register_message.argv.add(Register_input_username);
		Register_message.argv.add(Register_input_password);
		
		  try{
			  oos.writeObject(Register_message);
			  oos.flush();
			  
			  Register_response =  (MyMessage)ois.readObject();
			  
			  if(!Register_response.type.equals("register_response") || Register_response.args != 1 || Register_response.argv.size() != 1){
				  Register_result.setText("Register failed, please try again!");
				  Register_username.setText("");
				  Register_password.setText("");
				  Register_confirm_password.setText("");
				  return false;
			  }
			  else if(Register_response.argv.get(0).equals("success")){
				  Register_result.setText("Welcome to join us!");
				  return true;
			  }
			  else{
				  Register_result.setText(Register_response.argv.get(0));
				  Register_username.setText("");
				  Register_password.setText("");
				  Register_confirm_password.setText("");
				  return false;
			  }		  
		  }catch (Exception e){
			  e.printStackTrace();
			  Register_result.setText("Register failed, please try again!");
			  Register_username.setText("");
			  Register_password.setText("");
			  Register_confirm_password.setText("");
			  return false;
		  }
	}
	
	//buttons on click 
	public void onClick(View v) {
		    if(v == Register_join){
		    	if(check_input()){
		    		if(commit_register()){
		    			Register_result.setText("Register succeed!");
		    			Handler handler = new Handler(); 	
		                handler.postDelayed(new Runnable() {
		                    @Override
		                    public void run() {                     
		                    	Login_Activity.set_socket(Register_socket);
		                    	Login_Activity.set_stream(oos, ois);
		                    	startActivity(new Intent(Register_Activity.this, Login_Activity.class));
		                    	finish();
		                    }
		                }, 3000);  
		    		}
		    	}
		    }
		    if(v == Register_back){
		    	Login_Activity.set_socket(Register_socket);
		    	Login_Activity.set_stream(oos, ois);
		    	startActivity(new Intent(Register_Activity.this, Login_Activity.class));
		    	finish();
		    }
		  }
}
