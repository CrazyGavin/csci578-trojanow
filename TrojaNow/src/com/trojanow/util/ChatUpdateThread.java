package com.trojanow.util;

import java.io.BufferedReader;  
import java.io.IOException;  
import java.io.InputStreamReader;  
import java.io.ObjectInputStream;
import java.net.Socket;  

import com.trojanow.util.MyMessage;

import android.os.Handler;  
import android.os.Message;  
  
public class ChatUpdateThread implements Runnable {  
    private Handler handler;  
//    // 该线程所处理的Socket所对应的输入流  
//    private BufferedReader br = null;  
//    private Socket socket;
    private ObjectInputStream ois;
    
    public ChatUpdateThread(ObjectInputStream input, Handler handler) throws IOException {  
        this.handler = handler;  
        this.ois = input;
//        br = new BufferedReader(new InputStreamReader(socket.getInputStream(),"UTF-8"));  
    } 
  
    @Override  
    public void run() {  
        try {  
            MyMessage content = null;  

            while (!Thread.interrupted()) { 
            	
//            	Thread.sleep(1000);

            	if(ois.available()==0){
            		Object temp = ois.readObject();
            		content = (MyMessage)temp;
            	}else{
            		continue;
            	}
            	
            	if(content == null || !content.type.equals("chat_response") || content.args != 2 || content.argv.size() != 2 || !content.argv.get(0).equals("true")){
            		continue;
            	}
            	
            	if(content.argv.get(1).toString().equals("exit") || content.argv.get(1).toString().equals("enter")){
            		continue;
            	}
            	
            	Message msg = new Message();  
                msg.what = 0x234;  
                msg.obj = content.argv.get(1).toString(); 
                handler.sendMessage(msg);  
            }  
        } catch (Exception e) { 
            e.printStackTrace();  
        }  
    }  
  
} 
