package com.trojanow.util;

import java.util.List;

import com.trojanow.R;
import com.trojanow.activity.Comment_Activity;
import com.trojanow.activity.Main_Activity;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
 
public class ListViewAdapter extends BaseAdapter{
    private List<Status> items;
    private LayoutInflater inflater;
     
    public ListViewAdapter(Context context, List<Status> items) {
        this.items = items;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);      
    }

    @Override
    public int getCount() {
        return items.size();
    }
 
    @Override
    public Object getItem(int position) {
        return items.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.list_item, null);
        }

        TextView List_date = (TextView) view.findViewById(R.id.List_date);
        List_date.setText(items.get(position).getDate());
        
        TextView List_environment = (TextView) view.findViewById(R.id.List_environment);
        List_environment.setText(items.get(position).getEnvironment());
        
        TextView List_username = (TextView) view.findViewById(R.id.List_username);
        String temp = items.get(position).getUsername();
        if(temp.contains("Anonymous")){
        	List_username.setText("Anonymous");
        }else{
        	List_username.setText(temp);
        }
        
        TextView List_content = (TextView) view.findViewById(R.id.List_content);
        List_content.setText(items.get(position).getContent());
        
        TextView List_category = (TextView) view.findViewById(R.id.List_category);
        if(items.get(position).getTarget().toLowerCase().equals("all")){
        	List_category.setText("Public");
        }else{
        	List_category.setText("Private");
        }
        final Button comment;
        comment = (Button)view.findViewById(R.id.Status_Comment);
//        comment.setTag(position);
        comment.setOnClickListener(new View.OnClickListener() {          
        	@Override
        	public void onClick(View v) {
        		Comment_Activity.set_curstatus(items.get(position));
            	Message msg = new Message();  
                msg.what = 0x234;  
                msg.obj = "comment"; 
                Handler temp_handler = Main_Activity.get_handler();
                temp_handler.sendMessage(msg);  
        	} 
        });

        
        return view;
    }
     
    /**
     * �����б���
     * @param item
     */
    public void addItem(Status item) {
        items.add(item);
    }
}