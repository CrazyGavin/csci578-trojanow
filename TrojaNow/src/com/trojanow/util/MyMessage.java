package com.trojanow.util;

import java.util.ArrayList;

public class MyMessage implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1193459741493050046L;
	public String type;
	public int args;
	public ArrayList<String> argv;
	public ArrayList<Status> argvNews;
	
	public MyMessage(String t, int s){
		type = t;
		args = s;
		argv = new ArrayList<String>();
		argvNews = new ArrayList<Status>();
	}
}
