package com.trojanow.db;

import com.trojanow.db.Config;
import java.sql.*;
import java.util.ArrayList;

import com.trojanow.util.Status;

public class DatabaseAPI {
	public static Connection connect2DB(){
		Connection connect = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection(Config.dbURL, Config.dbUsername, Config.dbPassword);
		}catch(Exception e){
			e.printStackTrace();
		}
		return connect;
	}
	public static void closeConnection(Connection connect){
		try{
			connect.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return;
	}
	
	public static boolean checkUserName(Connection connect, String username){
		username = username.toLowerCase();
		try{
			Statement stmt = connect.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM userinf WHERE userName = '" + username + "'");
			if(!rs.next()){
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return true;
	}
	
	public static boolean checkPassWord(Connection connect, String username, String password){
		username = username.toLowerCase();
		try{
			Statement stmt = connect.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM userinf WHERE userName = '" + username + "' AND userPassword ='" + password + "'");
			if(rs.next()){
				return true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean saveRegister(Connection connect, String username, String password){
		username = username.toLowerCase();
		try{
			Statement stmt = connect.createStatement();
			stmt.execute("INSERT INTO userinf VALUES( '" + username + "' , '" + password + "')");
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean saveNews(Connection connect, Status input){
		try{
			Statement stmt = connect.createStatement();
			int statusCount = getMaxID(connect, "status");		
			if( -1 == statusCount ){
				return false;
			}
			stmt.execute("INSERT INTO status VALUES('" + (statusCount + 1) + "', '" + input.getUsername() + "' , '" + input.getDate() + "' , '" + input.getContent() + "' , '" + input.getEnvironment() + "' , '" + input.getTarget() + "')");
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static ArrayList<Status> getNews(Connection connect, String username, int range,  int count){
		ArrayList<Status> result = new ArrayList<Status>();
		
		try{
			Statement stmt = connect.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM status WHERE ID < '" + range +"' AND (statusTarget = 'all' OR statusTarget = '" + username  + "' OR statusUsername = 'Anonymous_" + username + "' OR statusUsername = '" + username + "') ORDER BY ID DESC LIMIT 0, " +count);
			while(rs.next()){
				Status temp = new Status(rs.getString("statusUsername"), rs.getString("statusDate"), rs.getString("statusContent"), rs.getString("statusEnvironment"), rs.getString("statusTarget"));
				temp.setID(rs.getInt("ID"));
				result.add(temp);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	private static int getMaxID(Connection connect, String table){
		try{
			Statement stmt = connect.createStatement();
			ResultSet rs = stmt.executeQuery("select max(id) as result from " + table);
			
			if(rs.next()){
				return rs.getInt("result");
			}
			else{
				return 0;
			}
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}
	
	public static boolean saveComments(Connection connect, String statusID, String userName, String date, String content){
		try{
			Statement stmt = connect.createStatement();
			int statusCount = getMaxID(connect, "comment");		
			if( -1 == statusCount ){
				return false;
			}
			
			if(content.equals("=like=") || content.equals("=dislike=")){
				ResultSet rs = stmt.executeQuery("SELECT * FROM comment WHERE statusID = " + statusID + " AND commentUsername = '" + userName + "' AND " + "commentContent = '" + content + "'");
				if(rs.next()){
					stmt.execute("DELETE FROM comment WHERE statusID = " + statusID + " AND commentUsername = '" + userName + "' AND " + "commentContent = '" + content + "'");
					return true;
				}
			}
			
			stmt.execute("INSERT INTO comment VALUES('" + (statusCount + 1) + "', '" + statusID + "' , '" + userName + "' , '" + date + "' , '" + content + "')");
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static ArrayList<String> getComment(Connection connect, String statusID){
		ArrayList<String> result = new ArrayList<String>();
		try{
			Statement stmt = connect.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM comment WHERE statusID = " + statusID);
			while(rs.next()){
				if(rs.getString("commentContent").equals("=like=") || rs.getString("commentContent").equals("=dislike=")){
					continue;
				}
				String temp = rs.getString("commentDate").split(" ")[0] + "\t" + rs.getString("commentUsername") + "\n" + rs.getString("commentContent") + "\n" ;
				result.add(temp);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public static int countComment(Connection connect, String statusID, String content){
		int result = 0;
		try{
			Statement stmt = connect.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS RESULT FROM comment WHERE statusID = " + statusID + " AND commentContent = '" + content + "'");
			while(rs.next()){
				result = rs.getInt("RESULT");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
}
