package com.trojanow.server;

import java.io.IOException;  
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;  
import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.trojanow.db.DatabaseAPI;
import com.trojanow.util.MyMessage;
import com.trojanow.util.Status;
  
public class ServerThread implements Runnable {  
    private Socket socket = null;  
    private Connection connect = null;
    
    private static Lock lock = new ReentrantLock();
    
    public ServerThread(Socket socket) throws IOException {  
        this.socket = socket;
    }  
  
    @Override  
    public void run() {  
    	
    	System.out.println(Thread.currentThread().getName() + ": client joined!");
    	try{
    		connect = DatabaseAPI.connect2DB();
    		System.out.println(Thread.currentThread().getName() + ": connect to Database....  Succeed!");
    	}catch(Exception e){
    		e.printStackTrace();
    		System.out.println(Thread.currentThread().getName() + ": connect to Database....  Failed!");
    	}
    	
        try {  
    		ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
    		ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
    		TrojaNowServer.oosMap.put(socket, oos);
        	while(true){
        		while(ois.available() < 0){
        			Thread.sleep(100);
        			continue;
        		}
        		MyMessage input =  (MyMessage)ois.readObject();        		
        		MyMessage output = handleMessage(input);
        		
        		if(output == null){
        			continue;
        		}
        		
        		oos.writeObject(output);
        		oos.flush();		
        	}
        } catch (Exception e) {  
            e.printStackTrace();  
        }
        TrojaNowServer.socketList.remove(socket);
        DatabaseAPI.closeConnection(connect);
        System.out.println(Thread.currentThread().getName() + ": client end!");
    }
    
    private MyMessage handleMessage(MyMessage input){
    	if(input.type.equals("login")){
    		return loginBehavior(input);
    	}
    	if(input.type.equals("register")){
    		return registerBehavior(input);
    	}
    	if(input.type.equals("mainLoadData")){
    		return loadDataBehavior(input);
    	}	
    	if(input.type.equals("publish")){
    		return publishBehavior(input);
    	}
    	if(input.type.equals("chat")){
    		return chatBehavior(input);
    	}
    	if(input.type.equals("comment_submit")){
    		return commentSubmitBehavior(input);
    	}
    	if(input.type.equals("comment_get")){
    		return commentGetBehavior(input);
    	}
    	if(input.type.equals("comment_count")){
    		return commentCountBehavior(input);
    	}
    	return null;
    }
    
    //check login
    private MyMessage loginBehavior(MyMessage input){
    	MyMessage output = new MyMessage("login_response",2);
    	if(input.type.equals("login") && input.args == 2 && input.argv.size() == 2)
    	{
    		lock.lock();
    		try{
	//    	  	  if((input.argv.get(0).equals("gavin") && input.argv.get(1).equals("loveshirley")) || input.argv.get(1).equals("")){	  
    			
    			if(!DatabaseAPI.checkUserName(connect, input.argv.get(0).toLowerCase())){
	    	  		output.argv.add("false");
	    	  		output.argv.add("Invalid username!");
	    	  		System.out.println(Thread.currentThread().getName()+ ": User not exist!");
    			}
    			else if(DatabaseAPI.checkPassWord(connect, input.argv.get(0).toLowerCase(), input.argv.get(1))){	  
	    	  		output.argv.add("true");
	    	  		output.argv.add("true");
	    	  		System.out.println(Thread.currentThread().getName()+ ": login succeed!");
	    	  	  }
	    	  	  else{
	    	  		output.argv.add("false");
	    	  		output.argv.add("Invalid password!");
	    	  		System.out.println(Thread.currentThread().getName()+ ": Password invalid!");
	    	  	  }
    			}
    		finally{
	    		lock.unlock();
    		}
    	}
    	else{
    		output.argv.add("false");
    		System.out.println(Thread.currentThread().getName() + ": login failed!");
    	}
    	return output;
    }
  
    //register message
    private MyMessage registerBehavior(MyMessage input){
    	MyMessage output = new MyMessage("register_response",1);
    	if(input.type.equals("register") && input.args == 2 && input.argv.size() == 2){
    		lock.lock();
    		try{
	    		if(!DatabaseAPI.checkUserName(connect, input.argv.get(0).toLowerCase())){
	    			if(DatabaseAPI.saveRegister(connect, input.argv.get(0).toLowerCase(), input.argv.get(1))){
	        			output.argv.add("success");
	        			System.out.println(Thread.currentThread().getName() + " register succeed!");	
	    			}else{
	       			 output.argv.add("Something wrong? Try again!");
	       			 System.out.println(Thread.currentThread().getName() + ": register failed!");
	    			}
	    		}else{
	    			 output.argv.add("Username already exist!");
	    			 System.out.println(Thread.currentThread().getName() + ": register failed!");
	    		}
    		}finally{
    			lock.unlock();
    		}
    	}
    	else{
    		output.argv.add("Register failed, please try again!");
    		System.out.println(Thread.currentThread().getName() + ": register failed!");
    	}
    	return output;
    }

    //load data message
    private MyMessage loadDataBehavior(MyMessage input){
    	MyMessage output = new MyMessage("loadData_response",1);
    	if(input.type.equals("mainLoadData") && input.args == 2 && input.argv.size() == 2){  
    		lock.lock();
    		try{
	    		ArrayList<Status> temp = DatabaseAPI.getNews(connect, input.argv.get(1).toLowerCase() , Integer.parseInt(input.argv.get(0)), 3);
	    		
	    		for(Status n:temp){
	    			output.argvNews.add(n);
	    		}
	    		
	    		output.argv.add("true");
	    		System.out.println(Thread.currentThread().getName()+ ": load data succeed!");
    		}finally{
    			lock.unlock();
    		}
    	}
    	
    	else{
    		output.argv.add("false");
    		System.out.println(Thread.currentThread().getName() + ": load data failed!");
    	}
    	return output;
    }
    
  //publish data
    private MyMessage publishBehavior(MyMessage input){
    	MyMessage output = new MyMessage("publish_response",1);
    	if(input.type.equals("publish") && input.args == 1 && input.argvNews.size() == 1){
    		lock.lock();
    		try{
	    		if(DatabaseAPI.saveNews(connect, input.argvNews.get(0))){
		    		output.argv.add("true");
		    		System.out.println(Thread.currentThread().getName() + ": publish succeed!");
	    		}
	    		else{
	        		output.argv.add("false");
	        		System.out.println(Thread.currentThread().getName() + ": publish failed!");
	    		}
    		}
    		finally{
    			lock.unlock();
    		}
    	}

    	else{
    		output.argv.add("false");
    		System.out.println(Thread.currentThread().getName() + ": publish failed!");
    	}
    	return output;
    }
    
    //chat message
    private MyMessage chatBehavior(MyMessage input){
    	MyMessage output = new MyMessage("chat_response",2);
    	if(input.type.equals("chat") && input.args == 2 && input.argv.size() == 2){
    		if(input.argv.get(0).equals("enter")){
    			TrojaNowServer.ID2SocketMap.put(input.argv.get(1).toLowerCase(), socket);
    			output.argv.add("true");
    			output.argv.add("enter");
    		}
    			
    		if(input.argv.get(0).equals("exit")){
    			TrojaNowServer.ID2SocketMap.remove(input.argv.get(1).toLowerCase());
    			output.argv.add("true");
    			output.argv.add("exit");
    		}
    	}
    	else if(input.type.equals("chat") && input.args == 3 && input.argv.size() == 3){
    		try{    			
    			if(input.argv.get(0).toString().equals("")){			
        			output.argv.add("true");
        			output.argv.add("[Public]\t" +input.argv.get(2).toString());   				
	    			for(Socket s: TrojaNowServer.socketList){
	    				if(socket.equals(s)){
	    					continue;
	    				}
	    				TrojaNowServer.oosMap.get(s).writeObject(output);
	    				TrojaNowServer.oosMap.get(s).flush();
	    			}
    			}
    			else{
        			output.argv.add("true");
        			if(!DatabaseAPI.checkUserName(connect, input.argv.get(0).toString().toLowerCase())){
        				output.argv.add("User: "+input.argv.get(0).toString()+" is not exist!");
        			}
        			else if(!TrojaNowServer.ID2SocketMap.containsKey(input.argv.get(0).toString().toLowerCase())){
        	    		output.argv.add("User: "+input.argv.get(0).toString()+" is not online!");
        			}
        			else{
        				output.argv.add("["+input.argv.get(0).toString()+"]\t" +input.argv.get(2).toString());
        				if(!input.argv.get(1).toString().toLowerCase().equals(input.argv.get(0).toString().toLowerCase())){   
	        				TrojaNowServer.oosMap.get(TrojaNowServer.ID2SocketMap.get(input.argv.get(0).toString())).writeObject(output);
	        				TrojaNowServer.oosMap.get(TrojaNowServer.ID2SocketMap.get(input.argv.get(0).toString())).flush();
        				}
        			}	
    			} 			
    			System.out.println(Thread.currentThread().getName() + ": " + input.argv.get(2).toString());
    		}catch (Exception e){
    			e.printStackTrace();
    		}
    	}
    	else{
    		output.argv.add("false");
    		output.argv.add(" ");
    		System.out.println(Thread.currentThread().getName() + ": chat publish failed!");
    	}
    		
    	return output;
    }
    
  //comment Submit
    private MyMessage commentSubmitBehavior(MyMessage input){
    	MyMessage output = new MyMessage("comment_submit_response",1);
    	if(input.type.equals("comment_submit") && input.args == 4 && input.argv.size() == 4)
    	{
    		lock.lock();
    		try{
        		if(DatabaseAPI.saveComments(connect, input.argv.get(0), input.argv.get(1), input.argv.get(2), input.argv.get(3))){	  
        	  		output.argv.add("true");
        	  		System.out.println(Thread.currentThread().getName()+ ": comment succeed!");
        	  	  }
        	  	  else{
        	  		output.argv.add("false");
        	  		System.out.println(Thread.currentThread().getName()+ ": comment failed!");
        	  	  }
    			}
    		finally{
        		lock.unlock();
    		}
    	}
    	else{
    		output.argv.add("false");
    		System.out.println(Thread.currentThread().getName() + ": comment publish failed!");
    	}
    	return output;
    }
    
  //comment  Get
    private MyMessage commentGetBehavior(MyMessage input){
    	MyMessage output = new MyMessage("comment_get_response",2);
    	if(input.type.equals("comment_get") && input.args == 1 && input.argv.size() == 1)
    	{
    		lock.lock();
    		try{
    			List<String> res = new LinkedList<String>();
    			res = DatabaseAPI.getComment(connect, input.argv.get(0));
    			if(res.isEmpty()){
    				output.argv.add("false");
    				output.argv.add(" ");
    				System.out.println(Thread.currentThread().getName() + ": comment get failed!");
    			}
    			else{
    				String comments = "";
    				
    				for(String s:res){
    					comments = s + comments + "\n";
    				}
    				
    				output.argv.add("true");
    				output.argv.add(comments);
    				System.out.println(Thread.currentThread().getName() + ": comment get succeed!");
    			}
    		}
    		finally{
        		lock.unlock();
    		}
    	}
    	else{
    		output.argv.add("false");
    		output.argv.add(" ");
    		System.out.println(Thread.currentThread().getName() + ": comment get failed!");
    	}
    	return output;
    }
    
    //comment  Count
    private MyMessage commentCountBehavior(MyMessage input){
    	MyMessage output = new MyMessage("comment_count_response",2);
    	if(input.type.equals("comment_count") && input.args == 2 && input.argv.size() == 2)
    	{
    		lock.lock();
    		try{
    			int res = DatabaseAPI.countComment(connect, input.argv.get(0), input.argv.get(1));
    				output.argv.add("true");
    				output.argv.add(res + "");
    				System.out.println(Thread.currentThread().getName() + ": comment get succeed!");
    		}
    		finally{
        		lock.unlock();
    		}
    	}
    	else{
    		output.argv.add("false");
    		output.argv.add(" ");
    		System.out.println(Thread.currentThread().getName() + ": comment get failed!");
    	}
    	return output;
    }
}  

