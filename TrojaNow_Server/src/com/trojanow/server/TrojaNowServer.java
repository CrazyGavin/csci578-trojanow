package com.trojanow.server;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class TrojaNowServer {
	public static ArrayList<Socket> socketList = new ArrayList<Socket>();  
	public static HashMap<String, Socket> ID2SocketMap = new HashMap<String, Socket>();
	public static HashMap<Socket, ObjectOutputStream> oosMap = new HashMap<Socket, ObjectOutputStream>();
	
	public static void main(String args[]) throws IOException{
		ServerSocket ss = new ServerSocket(20000);
		System.out.println("Server sets up Successful!");
		System.out.println("Waiting for connect ....");
		
		while(true){
			Socket socket = ss.accept();
			socketList.add(socket);
			
			try{
				new Thread(new ServerThread(socket)).start();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
}
