package com.trojanow.util;

public class Status  implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1569042638547643970L;
	
	private String News_username;
	private String News_date;
	private String News_content;
	private String News_environment;
	private String News_target;
	
	private int ID;
	
	public Status(String username, String date, String content, String environment, String target){
		News_username = username.replace("'", "").replace("\"", "''");
		News_date = date.replace("'", "").replace("\"", "''");
		News_content = content.replace("'", "").replace("\"", "''");
		News_environment = environment.replace("'", "").replace("\"", "''");
		News_target = target.replace("'", "").replace("\"", "''");
	}
	
	public String getUsername(){
		return News_username;
	}
	
	public String getDate(){
		return News_date;
	}
	
	public String getContent(){
		return News_content;
	}
	
	public String getEnvironment(){
		return News_environment;
	}
	
	public String getTarget(){
		return News_target;
	}
	
	public void setID(int input){
		ID = input;
	}
	
	public int getID(){
		return ID;
	}
	
	public Status copy(){
		return (new Status(News_username,News_date,News_content,News_environment, News_target));
	}
}
